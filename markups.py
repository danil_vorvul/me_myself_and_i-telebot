from telebot import types


### BACK BUTTON ###
button_back = types.KeyboardButton("Back to main menu")
back_to_main_menu = types.ReplyKeyboardMarkup(
    resize_keyboard=True, one_time_keyboard=True
)
back_to_main_menu.add(button_back)

### SEX BUTTONS ###
button_male = types.KeyboardButton("Male")
button_female = types.KeyboardButton("Female")
sex = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
sex.row(button_male, button_female, button_back)

### SETTINGS MENU ###
button_change_name = types.KeyboardButton("Change name")
button_change_sex = types.KeyboardButton("Change sex (Wow)")
button_change_age = types.KeyboardButton("Change age")
settings_menu = types.ReplyKeyboardMarkup(resize_keyboard=True)
settings_menu.add(button_change_name)
settings_menu.add(button_change_sex)
settings_menu.add(button_change_age)
settings_menu.add(button_back)

### MAIN MENU ###
button_whoiam = types.KeyboardButton("Хто я ?")
button_settings = types.KeyboardButton("Settings")
main_menu = types.ReplyKeyboardMarkup(resize_keyboard=True)
main_menu.add(button_settings)
main_menu.add(button_whoiam)
