# Myself telegram bot 
[![Heroku](https://www.herokucdn.com/deploy/button.svg)](https://arcane-savannah-11824.herokuapp.com/)
[![Code style: Black](https://img.shields.io/badge/code%20style-black-000000.svg?style=for-the-badge&logo=appveyor)](https://github.com/psf/black)
[![Linter: Flake8](https://img.shields.io/badge/Linter-Flake8-9cf?style=for-the-badge&logo=appveyor)](https://pypi.org/project/flake8/)
[![Type Checker: PyRight](https://img.shields.io/badge/type%20checker-PyRight-9cf?style=for-the-badge&logo=appveyor)](https://github.com/microsoft/pyright)

## Fast overview
This telegram bot uses flask, mongodb, telebot to store your data

## How to use
1. You need to clone it from repo
```
git clone https://gitlab.com/danil_vorvul/me_myself_and_i-telebot.git
cd me_myself_and_i-telebot
```
2. Set settings variables in settings.py and save the file (also you have to create bot in @BotFather and get api token)
```
vim settings.py
```
3. Create your own heroku app
```
heroku login
heroku create
```
4. Now you can deploy your app
```
git add .
git commit -m "First deploy"
git push heroku master
```
5. Congratulations you have deployed your first telegram bot! You can flex.
