import messages, settings, markups
from db import users_db, client
from telebot import types
from bot import bot


@bot.message_handler(commands=["start"])
def start_bot(message):
    if not users_db.find_one({"chat_id": message.chat.id}):
        users_db.insert_one({"chat_id": message.chat.id})

        bot.send_message(message.chat.id, messages.HELLO)
        bot.send_message(message.chat.id, messages.NAME)

        bot.register_next_step_handler(message, set_name)
    else:
        msg = bot.send_message(
            message.chat.id, messages.BACK, reply_markup=markups.settings_menu
        )
        bot.register_next_step_handler(msg, main_menu)

def set_age(message):
    if not str(message.text).isdigit():
        msg = bot.send_message(message.chat.id, messages.INVALID_AGE)
        bot.register_next_step_handler(msg, set_age)
        return

    user_age = int(message.text)
    users_db.update({"chat_id": message.chat.id}, {"$set": {"age": user_age}})
    msg = bot.send_message(
        message.chat.id, messages.END, reply_markup=markups.main_menu
    )

    bot.register_next_step_handler(message, main_menu)

def set_name(message):
    username = str(message.text)

    if len(username) < 2 or len(username) > 20:
        msg = bot.send_message(message.chat.id, messages.INVALID_NAME)
        bot.register_next_step_handler(msg, set_name)
        return

    username = username.title()
    users_db.update({"chat_id": message.chat.id}, {"$set": {"name": username}})

    bot.send_message(message.chat.id, messages.CORECT_NAME)
    msg = bot.send_message(message.chat.id, messages.SEX, reply_markup=markups.sex)
    bot.register_next_step_handler(msg, set_sex)

def set_sex(message):
    user_sex = str(message.text)

    if user_sex not in settings.SEXES:
        msg = bot.send_message(message.chat.id, messages.INVALID_SEX)
        bot.register_next_step_handler(msg, set_sex, reply_markup=markups.sex)
        return

    users_db.update({"chat_id": message.chat.id}, {"$set": {"sex": user_sex}})

    bot.send_message(message.chat.id, messages.AGE)
    bot.register_next_step_handler(message, set_age)

def change_name(message):
    message_str = str(message.text)

    if message_str.lower() == "back to main menu":
        msg = bot.send_message(
            message.chat.id, messages.BACK, reply_markup=markups.settings_menu
        )
        bot.register_next_step_handler(message, settings_menu)
    else:
        if len(username) < 2 or len(username) > 20:
            msg = bot.send_message(message.chat.id, messages.INVALID_NAME)
            bot.register_next_step_handler(msg, change_name)
            return

        message_str = message_str.title()
        users_db.update({"chat_id": message.chat.id}, {"$set": {"name": message_str}})
        msg = bot.send_message(
            message.chat.id, messages.CORECT_NAME, reply_markup=markups.settings_menu
        )
        bot.register_next_step_handler(msg, settings_menu)

def change_sex(message):
    message_str = str(message.text)
    if message_str.lower() == "back to main menu":
        msg = bot.send_message(
            message.chat.id, messages.BACK, reply_markup=markups.settings_menu
        )
        bot.register_next_step_handler(msg, settings_menu)
    else:
        users_db.update({"chat_id": message.chat.id}, {"$set": {"sex": message_str}})
        msg = bot.send_message(
            message.chat.id, messages.CORECT_SEX, reply_markup=markups.settings_menu
        )
        bot.register_next_step_handler(msg, settings_menu)

def change_age(message):
    message_str = str(message.text)
    if message_str.lower() == "back to main menu":
        msg = bot.send_message(
            message.chat.id, messages.BACK, reply_markup=markups.settings_menu
        )
        bot.register_next_step_handler(msg, settings_menu)
    else:
        try:
            if not str(message.text).isdigit():
                msg = bot.send_message(message.chat.id, messages.INVALID_AGE)
                bot.register_next_step_handler(msg, change_age)
                return

            message_int = int(message_str)
            users_db.update(
                {"chat_id": message.chat.id}, {"$set": {"age": message_int}}
            )
            msg = bot.send_message(
                message.chat.id, messages.AGE_CORECT, reply_markup=markups.settings_menu
            )

            bot.register_next_step_handler(msg, settings_menu)
        except TypeError:
            msg = bot.send_message(
                message.chat.id,
                messages.ERROR_AGE,
                reply_markup=markups.back_to_main_menu,
            )
            bot.register_next_step_handler(msg, change_age)


def main_menu(message):
    button_message = str(message.text)
    button_message = button_message.lower()

    if button_message == "хто я ?":
        user = users_db.find_one({"chat_id": message.chat.id})
        msg = bot.send_message(
            message.chat.id,
            "Your name : {} \nYour age : {} \nYour sex : {}".format(
                user.get("age", ""), user.get("name", ""), user.get("sex", "")
            ),
            reply_markup=markups.main_menu,
        )
        bot.register_next_step_handler(msg, main_menu)
    elif button_message == "settings":
        msg = bot.send_message(
            message.chat.id,
            messages.SWITCH_TO_SETTINGS_MENU,
            reply_markup=markups.settings_menu,
        )
        bot.register_next_step_handler(message, settings_menu)
    else:
        bot.register_next_step_handler(message, main_menu)


def settings_menu(message):
    button_message = str(message.text)
    button_message = button_message.lower()

    if button_message == "change name":
        msg = bot.send_message(
            message.chat.id, messages.NAME, reply_markup=markups.back_to_main_menu
        )
        bot.register_next_step_handler(msg, change_name)

    elif button_message == "change sex (wow)":
        msg = bot.send_message(message.chat.id, messages.SEX, reply_markup=markups.sex)
        bot.register_next_step_handler(msg, change_sex)

    elif button_message == "change age":
        msg = bot.send_message(
            message.chat.id, messages.AGE, reply_markup=markups.back_to_main_menu
        )
        bot.register_next_step_handler(msg, change_age)

    elif button_message == "back to main menu":
        msg = bot.send_message(message.chat.id, messages.SWITCH_TO_MAIN_MENU, reply_markup=markups.main_menu)
        bot.register_next_step_handler(message, main_menu)

    else:
        bot.register_next_step_handler(message, settings_menu)


if __name__ == "__main__":
    bot.remove_webhook()
    bot.polling(none_stop=True)
