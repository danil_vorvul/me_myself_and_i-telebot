import os

HEROKU_URL = os.getenv("APP_URL")
MONGODB_URL = os.getenv("MONGO_URL")
TOKEN = "<MUST BE SET!>"
SEXES = ("Male", "Female")
