import pymongo
from settings import MONGODB_URL


client = pymongo.MongoClient(MONGODB_URL, retryWrites=False)
users_db = client.get_default_database()["myself_users"]
